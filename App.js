import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('./samplePic.jpg')}
          style={styles.image}
          alt=""
        />
        <Text style={styles.columnB}>
          <Text>Laura,</Text>
          <Text>27</Text>
        </Text>
        <Text style={styles.columnC}>
          <Text style={{ display: 'flex', flex:3, width: 10, height: 10}}> 93% </Text>
          <Text style={{ display: 'flex', flex: 3, width: 10, height: 10}}> match </Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 25,
    paddingLeft: 12,
    borderColor: 'red',
    borderStyle: 'solid',
    borderWidth: 3,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  columnB: {
    borderColor: 'blue',
    borderStyle: 'solid',
    borderWidth: 3,
    flex: 1
  },
  columnC: {
    borderColor: 'purple',
    borderStyle: 'solid',
    borderWidth: 3,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  titleText: {
    fontSize: 60,
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 50
  }
});
